package com.tsystems.javaschool.tasks.calculator;


import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private TokenStack operatorStack;
    private TokenStack valueStack;
    private boolean error;

    public Calculator() {
        operatorStack = new TokenStack();
        valueStack = new TokenStack();
        error = false;
    }

    private void processOperator(Token t) throws ArithmeticException{
        Token A = null, B = null;
        if (valueStack.isEmpty()) {
            error = true;
            return;
        } else {
            B = valueStack.top();
            valueStack.pop();
        }
        if (valueStack.isEmpty()) {
            error = true;
            return;
        } else {
            A = valueStack.top();
            valueStack.pop();
        }
        Token R = t.operate(A.getValue(), B.getValue());
        if (R == null)
            throw new ArithmeticException();
        valueStack.push(R);
    }

    public String evaluate(String statement) {
        // TODO: Implement the logic here
        //String[] parts = input.split(" ");
        if ((statement == null) || (statement.equals("")))
            return null;
        List<String> parts = new LinkedList<>();
        Pattern pattern = Pattern.compile("([0-9]+\\.[0-9]+|[0-9]+)|(\\+|\\-|\\(|\\)|\\*|\\/)");
        Matcher matcher = pattern.matcher(statement);
        while (matcher.find()) {
            parts.add(matcher.group());
        }
        Token[] tokens = new Token[parts.size()];
        for (int n = 0; n < parts.size(); n++) {
            tokens[n] = new Token(parts.get(n));
        }
        // Main loop - process all input tokens
        for (int n = 0; n < tokens.length; n++) {
            Token nextToken = tokens[n];
            if (nextToken.getType() == Token.NUMBER) {
                valueStack.push(nextToken);
            } else if (nextToken.getType() == Token.OPERATOR) {
                if (operatorStack.isEmpty() || nextToken.getPrecedence() > operatorStack.top().getPrecedence()) {
                    operatorStack.push(nextToken);
                } else {
                    while (!operatorStack.isEmpty() && nextToken.getPrecedence() <= operatorStack.top().getPrecedence()) {
                        Token toProcess = operatorStack.top();
                        operatorStack.pop();
                        try {
                            processOperator(toProcess);
                        }catch (ArithmeticException e){
                            return null;
                        }

                    }
                    operatorStack.push(nextToken);
                }
            } else if (nextToken.getType() == Token.LEFT_PARENTHESIS) {
                operatorStack.push(nextToken);
            } else if (nextToken.getType() == Token.RIGHT_PARENTHESIS) {
                while (!operatorStack.isEmpty() && operatorStack.top().getType() == Token.OPERATOR) {
                    Token toProcess = operatorStack.top();
                    operatorStack.pop();
                    try {
                        processOperator(toProcess);
                    }catch (ArithmeticException e){
                        return null;
                    }

                }
                if (!operatorStack.isEmpty() && operatorStack.top().getType() == Token.LEFT_PARENTHESIS) {
                    operatorStack.pop();
                } else {
                    error = true;
                    return null;
                }
            }

        }
        // Empty out the operator stack at the end of the input
        while (!operatorStack.isEmpty() && operatorStack.top().getType() == Token.OPERATOR) {
            Token toProcess = operatorStack.top();
            operatorStack.pop();
            try {
                processOperator(toProcess);
            }catch (ArithmeticException e){
                return null;
            }

        }
        // Print the result if no error has been seen.
        if(!error) {
            Token result = valueStack.top();
            valueStack.pop();
            if (!operatorStack.isEmpty() || !valueStack.isEmpty()) {
                return null;
            } else {
                NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
                DecimalFormat df = (DecimalFormat)nf;
                df.applyPattern("#.####");
                String formatted = df.format( result.getValue());
                return formatted;
            }
        }
        return null;
    }

}
