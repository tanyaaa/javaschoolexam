package com.tsystems.javaschool.tasks.pyramid;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException{
        // TODO : Implement your solution here
        try {
            Collections.sort(inputNumbers);
        } catch (OutOfMemoryError | NullPointerException e){
            throw new CannotBuildPyramidException();
        }

        int index = 0;
        int step = 1, sum = 0, count;
        do{
            count = 0;
            for (int i = index; i < index + step; i++){
                count++;
            }
            index += step;
            step++;
            sum+=count;
        }while (index < inputNumbers.size() - 1);
        if (sum != inputNumbers.size())
            throw new CannotBuildPyramidException();

        int[][] resultArray = new int[count][count*2 - 1];
        int shift = 0, m = count*2 - 2, n = count - 1, k = 0;
        index = inputNumbers.size() - 1;

        for (int i = n; i >= 0; i--){
            for (int j = m - shift; j >= 0; j-=2){
                if( index >= 0)
                    resultArray[i][j] = inputNumbers.get(index);
                k++;
                index--;
                if (k == count)
                    break;
            }
            shift++;
            count--;
            k = 0;
        }
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= m; j++)
                System.out.print(resultArray[i][j] + " ");
            System.out.println();
        }
        return resultArray;
    }


}
