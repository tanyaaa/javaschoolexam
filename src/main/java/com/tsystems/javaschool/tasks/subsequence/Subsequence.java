package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException{
        
        if (x == null | y == null)
            throw new IllegalArgumentException();
        int index = -1;
        boolean found = false;
        for (Object o1 : x){
            for (Object o2 : y){
                if (o1.equals(o2) && (y.indexOf(o2) > index)){
                   found = true;
                   index = y.indexOf(o2);
                   break;
                }
            }
            if (!found)
                return false;
            found = false;
        }
        return true;
    }
}
